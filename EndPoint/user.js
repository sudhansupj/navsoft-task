export const userLogin = (email,password) => {
    let  _allUser = window.localStorage.getItem('allUser') || ''
    _allUser =JSON.parse(_allUser);
   let _user =  _allUser.find(Element => Element.email === email && Element.password === password);
    if (_user ) return new Promise(resolve => resolve(_user));
    else return new Promise(resolve => resolve(''));
};

export const userSignUp = async (name,phone,email,dob,address,password,skill) => {
    let  _allUser = window.localStorage.getItem('allUser') || []
    _allUser =JSON.parse(_allUser);
    let data = {
        name,phone,email,dob,address,password,skill
    }
    _allUser.push(data);
    window.localStorage.setItem('allUser', JSON.stringify(_allUser));
    window.localStorage.setItem('user', JSON.stringify(data));
    window.localStorage.setItem('userLoggedIn', 'true');
    if (_allUser) return new Promise(resolve => resolve(_allUser));
};

export const getAllLoginUser = () => {
    const _allUser = window.localStorage.getItem('allUser') || ''

    if (_allUser) return new Promise(resolve => resolve(JSON.parse(_allUser)));
    else return  new Promise(resolve => resolve(JSON.parse([])))

};

