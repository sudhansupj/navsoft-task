/* eslint-disable no-use-before-define */

import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

export default function SkillsAutocomplete({onSelect}) {
    return (
        <Autocomplete
            multiple
            onChange={(event, newValue) => {
                if(newValue){
                    onSelect(newValue ? newValue : null);
                }
            }}
            id="checkboxes-tags-demo"
            options={skills}
            disableCloseOnSelect
            getOptionLabel={(option) => option.title}
            renderOption={(option, { selected }) => (
                <React.Fragment>
                    <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 8 }}
                        checked={selected}
                    />
                    {option.title}
                </React.Fragment>
            )}
            renderInput={(params) => (
                <TextField {...params} variant="outlined"  placeholder="skills" />
            )}
        />
    );
}

// Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
const skills = [
    { title: 'Verbal Communication ' },
    { title: 'Body Language ' },
    { title: 'Attention' },
    { title: 'Physical Communication ' },
    { title: 'Writing' },
    { title: 'Storytelling' },
    { title: 'Humor' },
    { title: 'Listening' },
    { title: 'Team Building ' },
];
