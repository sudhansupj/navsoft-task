import React, {useEffect, useState} from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import {useRouter} from 'next/router';
import {Box} from '@material-ui/core';
import Link from '../../src/Link';
import CoverImage from '../../public/LoginCoverImage.png';
import FormLabel from "@material-ui/core/FormLabel";
import {userLogin} from "../../EndPoint/user";
import dynamic from "next/dist/next-server/lib/dynamic";

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: typeof window !== "undefined" ? window.innerHeight : '-webkit-fill-available',
        display: 'flex',
    },
    paper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flex: '641',
    },
    bgImage: {
        backgroundImage: `url(${CoverImage})`,
        backgroundSize: 'cover',
        flex: '725',
        [theme.breakpoints.down('sm')]: {
        display: 'none',
        },
    },
    image: {
        height: '40px',
        marginBottom: '17px',
    },
    label: {
        marginBottom: '25px',
        color: theme.palette.text.main,
        fontSize: '15px',
        fontWeight: '500',
        letterSpacing: '0.3px',
    },
    usernameTextField: {
        marginBottom: theme.spacing(2)
    },
    link: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: '16px',
        marginBottom: '16px',
        fontSize: '13px',
    },
    icon: {
        width: '17px',
        height: 'auto',
        margin: '5px',
    },
    buttonContent: {
        fontSize: '12px',
        fontWeight: '400',
        letterSpacing: '0.3px',
    },
}));


const Login = () => {

    const classes = useStyles();


    const [email, setEmail] = React.useState('');
    const [emailError, setEmailError] = React.useState('');

    const [password, setPassword] = React.useState('');
    const [passwordError, setPasswordError] = React.useState('');

    const [showPassword, setShowPassword] = React.useState(false);

    const [loading, setLoading] = useState(false);

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    function validateUserName() {
        if(email === ''){
            setEmailError('Email can\'t be blank');
            return false;
        }else {
            setEmailError('');
            return true;
        }
    }

    function validatePassword() {
        if(password === ''){
            setPasswordError('Password can\'t be blank');
            return false;
        }else {
            setPasswordError('');
            return true;
        }
    }

    const handleLogin = () => {
        if(validateUserName() && validatePassword()){
            setLoading(true);
            userLogin(email,password)
                .then((response) => {
                   if(response) {
                       window.localStorage.setItem('user', JSON.stringify(response))
                       window.location.href = '/'
                   }
                })
        }
    };


    let  user = JSON.parse(window.localStorage.getItem('user') || null)
    if(user) window.location.href = '/'


    return(
        <div className={classes.root}>
            <div className={classes.bgImage}/>
            <div className={classes.paper} >
                <Box maxWidth="319px" width="90%">
                    {/*<img src={Logo} alt="Login Logo" className={classes.image}/>*/}
                    <Typography component="h1" variant="h4">
                        {'Sign in'}
                    </Typography>
                    <Typography className={classes.label}>
                        {'Dont have an account?'}
                        <Link href="/signup">
                            {' Sign Up'}
                        </Link>
                    </Typography>
                    <FormLabel>{'Email Address'}</FormLabel>
                    <TextField
                        autoFocus
                        fullWidth
                        onChange={event => setEmail(event.target.value)}
                        required
                        error={!!emailError}
                        helperText={emailError}
                        size="medium"
                        type={'email'}
                        className={classes.usernameTextField}
                        variant="outlined"
                        value={email}
                        // onKeyDown={handleEnter}
                    />
                    <FormLabel>{'Password'}</FormLabel>
                    <TextField
                        fullWidth
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={handleClickShowPassword}
                                    >
                                        {showPassword ? <VisibilityIcon/> : <VisibilityOffIcon/>}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                        size="medium"
                        error={!!passwordError}
                        helperText={passwordError}
                        name="password"
                        onChange={event => setPassword(event.target.value)}
                        required
                        variant="outlined"
                        type={showPassword ? 'text' : 'password'}
                        value={password}
                        // onKeyDown={handleEnter}
                    />
                    <Button
                        color="primary"
                        disabled={loading}
                        onClick={handleLogin}
                        variant="contained"
                        type="submit"
                        fullWidth
                        style={{marginTop:20}}
                    >
                        {loading ? <CircularProgress
                            size={24}
                        /> : 'Login'}
                    </Button>
                </Box>
            </div>
        </div>
    );
};

export default dynamic(() => Promise.resolve(Login), {
    ssr: false
})