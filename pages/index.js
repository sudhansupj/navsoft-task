import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import AppBar from "../Component/AppBar";
import {useRouter} from "next/router";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
        marginTop:40
    },
});

export default function Index() {
    const classes = useStyles();
    const Router = useRouter();

    const [allUser,setAllUser] = React.useState([]);
    const [user,setUser] = React.useState(null);

    function handleLogout(){
        window.localStorage.setItem('user', JSON.stringify(null));
        window.location.href = '/login'
    }

    useEffect(() =>{
        let  user = JSON.parse(window.localStorage.getItem('user') || null)
        let  allUser = JSON.parse(window.localStorage.getItem('allUser') || null)
        if(!user) return  window.location.href = '/login'
        else{
            setAllUser(allUser);
            setUser(user);
        }
    },[])


    if(!user) return ''

    console.log('allUser',allUser)

    return (
        <>
            <AppBar handleLogout={handleLogout}/>
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell align="right">Email</TableCell>
                        <TableCell align="right">Phone</TableCell>
                        <TableCell align="right">Dob</TableCell>
                        <TableCell align="right">Skills</TableCell>
                        <TableCell align="right">Address</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {allUser && !!allUser.length && allUser.map((row) => (
                        <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                                {row.name}
                            </TableCell>
                            <TableCell align="right">{row.email}</TableCell>
                            <TableCell align="right">{row.phone}</TableCell>
                            <TableCell align="right">{row.dob}</TableCell>
                            <TableCell align="right">
                                {
                                    row.skill && !!row.skill.length && row.skill.map((each,i) =>
                                        <Typography key={i}>
                                            {each.title}
                                        </Typography>
                                    )
                                }
                            </TableCell>
                            <TableCell align="right">{row.address}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
            </>
    );
}

