import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import Button from '@material-ui/core/Button';
import {useRouter} from 'next/router';
import Box from '@material-ui/core/Box';
import Link from '../../src/Link';
import CoverImage from "../../public/SignUpCoverImage.png";
import FormLabel from "@material-ui/core/FormLabel";
import SkillsAutocomplete from "../../Component/SkillsAutocomplete";
import {userSignUp} from "../../EndPoint/user";
import dynamic from 'next/dynamic'

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: typeof window !== "undefined" ? window.innerHeight : '-webkit-fill-available',
        display: 'flex',
    },
    paper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flex: '641',
    },
    bgImage: {
        backgroundImage: `url(${CoverImage})`,
        backgroundSize: 'cover',
        flex: '725',
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    },
    image: {
        height: '40px',
        marginBottom: '17px',
    },
    label: {
        marginBottom: '25px',
        color: theme.palette.text.main,
        fontSize: '15px',
        fontWeight: '500',
        letterSpacing: '0.3px',
    },
    link: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: '16px',
        marginBottom: '16px',
        fontSize: '13px',
    },
    icon: {
        width: '17px',
        height: 'auto',
        margin: '5px',
    },
    button: {
        marginTop: '16px',
    },
    buttonContent: {
        fontSize: '12px',
        fontWeight: '400',
        letterSpacing: '0.3px',
    },
}));

const SignUp = () => {

    const classes = useStyles();


    let  user = JSON.parse(window.localStorage.getItem('user') || null)
    if(user )window.location.href = '/'


    const [name, setName] = React.useState('');
    const [nameError, setNameError] = React.useState('');

    const [phone, setPhone] = React.useState('');

    const [email, setEmail] = React.useState('');
    const [emailError, setEmailError] = React.useState('');

    const [dob, setDob] = React.useState('');

    const [address, setAddress] = React.useState('');

    const [skills, setSkills] = React.useState([]);

    const [password, setPassword] = React.useState('');
    const [passwordError, setPasswordError] = React.useState('');

    const [confirmPassword, setConfirmPassword] = React.useState('');
    const [confirmPasswordError, setConfirmPasswordError] = React.useState('');

    const [showPassword, setShowPassword] = React.useState(false);
    const [loading, setLoading] = React.useState(false);

    const Router = useRouter();

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    function validateUserName() {
        if(email === ''){
            setEmailError('Email can\'t be blank');
            return false;
        }else {
            setEmailError('');
            return true;
        }
    }

    function validateName() {
        if(name === '') {
            setNameError('Name can\'t be blank');
            return false;
        }
        if(name.length < 3 ){
            setNameError('enter valid Name');
            return false;
        }else {
            setNameError('');
            return true;
        }
    }

    function validatePassword() {
        if(password === '') {
            setPasswordError('Password can\'t be blank');
            return false;
        }
      else if(password.length < 8){
            setPasswordError('Password length must be 8 character');
            return false;
        }else {
            setPasswordError('');
            return true;
        }
    }
    function validateConfirmPassword() {
        if(confirmPassword === ''){
            setConfirmPasswordError('Password can\'t be blank');
            return false;
        }
        else if(confirmPassword !== password) {
            setConfirmPasswordError('Password not match');
            return false;
        }
        else {
                setConfirmPasswordError('');
            return true;
        }
    }

    const handleRegister = () => {
        if(validateName() && validateUserName() && validatePassword() && validateConfirmPassword()) {
            setLoading(true);
            userSignUp(name,phone,email,dob,address,password,skills)
                .then(() =>{
                    setLoading(false);
                })
        }
    };

    const handleEnter = (event) => {
        if (event.keyCode === 13) {
            handleRegister();
        }
    };



    return(
        <div className={classes.root}>
            <div className={classes.bgImage}/>
            <div className={classes.paper}>
                <Box maxWidth="319px" width="90%">
                    <Typography component="h1" variant="h4">
                        {'Sign Up'}
                    </Typography>
                    <Typography className={classes.label}>
                        {'Already have an account?'}
                        <Link href="/login">
                            {' Sign In'}
                        </Link>
                    </Typography>
                    <FormLabel>{'Name'}</FormLabel>
                    <TextField
                        autoFocus
                        fullWidth
                        error={!!nameError}
                        helperText={nameError}
                        size="medium"
                        onChange={event => setName(event.target.value)}
                        required
                        variant="outlined"
                        value={name}
                        onKeyDown={handleEnter}
                    />
                    <FormLabel>{'Email'}</FormLabel>
                    <TextField
                        fullWidth
                        error={!!emailError}
                        helperText={emailError}
                        // label="Email"
                        // margin="normal"
                        size="medium"
                        onChange={event => setEmail(event.target.value)}
                        required
                        // type={email}
                        variant="outlined"
                        value={email}
                        onKeyDown={handleEnter}
                    />
                    <FormLabel>{'Phone'}</FormLabel>
                    <TextField
                        fullWidth
                        size="medium"
                        onChange={event => setPhone(event.target.value)}
                        required
                        // type={number}
                        variant="outlined"
                        value={phone}
                        onKeyDown={handleEnter}
                    />
                    <FormLabel>{'Birthday'}</FormLabel>
                    <TextField
                        fullWidth
                        id="date"
                        type="date"
                        defaultValue="2017-05-24"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        size="medium"
                        onChange={event => setDob(event.target.value)}
                        required
                        // type={email}
                        variant="outlined"
                        value={dob}
                        onKeyDown={handleEnter}
                    />
                    <FormLabel>{'Skills'}</FormLabel>
                    <SkillsAutocomplete onSelect={(data) => setSkills([...skills,...data])}/>
                    <FormLabel>{'Address'}</FormLabel>
                    <TextField
                        fullWidth
                        // label="Email"
                        // margin="normal"
                        size="medium"
                        onChange={event => setAddress(event.target.value)}
                        required
                        // type={email}
                        variant="outlined"
                        value={address}
                        onKeyDown={handleEnter}
                    />
                    <FormLabel>{'Password'}</FormLabel>
                    <TextField
                        fullWidth
                        // label="Password"
                        error={!!passwordError}
                        helperText={passwordError}
                        // margin="normal"
                        name="password"
                        onChange={event => setPassword(event.target.value)}
                        required
                        variant="outlined"
                        size="medium"
                        type={'password'}
                        value={password}
                        onKeyDown={handleEnter}
                    />
                    <FormLabel>{'Confirm Password'}</FormLabel>
                    <TextField
                        fullWidth
                        error={!!confirmPasswordError}
                        helperText={confirmPasswordError}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={handleClickShowPassword}
                                    >
                                        {showPassword ? <VisibilityIcon/> : <VisibilityOffIcon/>}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                        // label="Password"
                        // margin="normal"
                        name="password"
                        onChange={event => setConfirmPassword(event.target.value)}
                        required
                        variant="outlined"
                        size="medium"
                        type={showPassword ? 'text' : 'password'}
                        value={confirmPassword}
                        onKeyDown={handleEnter}
                    />
                    <Button
                        className={classes.button}
                        color="primary"
                        disabled={loading}
                        onClick={handleRegister}
                        variant="contained"
                        type="submit"
                        fullWidth
                    >
                        {loading ? <CircularProgress
                            size={24}
                        /> : 'Sign up'}
                    </Button>
                </Box>
            </div>
        </div>
    );
};


export default dynamic(() => Promise.resolve(SignUp), {
    ssr: false
})